﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRound.Exceptions
{
    public class StreetParserException : Exception
    {
        public StreetParserException(string message) : base(message)
        {
        }
    }
}
