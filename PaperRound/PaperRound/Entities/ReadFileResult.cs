﻿using System;

namespace PaperRound
{
    public class ReadFileResult : IReadFileResult
    {
        public ReadFileResult(bool success, string errorMessage)
        {
            this.Success = success;
            this.ErrorMessage = errorMessage;
        }

        public bool Success { get; set; }

        public string ErrorMessage { get; set; }
    }
}