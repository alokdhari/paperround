﻿namespace PaperRound
{
    public class House
    {
        public int Number { get; set; }

        public Directions Direction {
            get
            {
                return Number % 2 != 0 || Number == 1 ? Directions.North : Directions.South;
            }
        }
    }
}
