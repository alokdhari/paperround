﻿using System.Collections.Generic;
using System.Linq;

namespace PaperRound
{
    public class Street
    {
        public IList<House> Houses { get; set; }

        public IList<House> HousesOnNorth
        {
            get
            {
                return this.Houses.Where(o => o.Direction == Directions.North).ToList();
            }
        }
        
        public IList<House> HousesOnSouth
        {
            get
            {
                return this.Houses.Where(o => o.Direction == Directions.South).ToList();
            }
        }
    }
}
