﻿using System.Collections.Generic;
using System.Linq;

namespace PaperRound
{
    public class DeliverPaperBySideOfStreet : IPaperDelivery
    {
        public IList<House> GetDeliveryOrder(IList<House> housesInStreet)
        {
            var deliveryOrder = new List<House>();
            deliveryOrder.AddRange(housesInStreet.Where(o => o.Direction == Directions.North));
            deliveryOrder.AddRange(housesInStreet.Where(o => o.Direction == Directions.South).OrderByDescending(o => o.Number));
            return deliveryOrder;
        }

        public int TimeRoadCrossed(IList<House> housesInStreet)
        {
            // Always crossing only once.
            return 1;
        }
    }
}
