﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace PaperRound
{
    public class TownPlanner : ITownPlanner
    {
        private IFileReader fileReader;
        private StreetParser streetParser;

        public TownPlanner()
        {
            this.fileReader = new FileReader();
            this.streetParser = new StreetParser();
        }

        public Street GetStreetFromTextFile(string filePath)
        {
            var text = this.fileReader.ReadFile(filePath);
            return this.streetParser.ValidateAndParse(text);
        }
    }
}