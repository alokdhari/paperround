﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRound
{

    public class DeliverPaperByOrderOfHouses : IPaperDelivery
    {
        public IList<House> GetDeliveryOrder(IList<House> housesInStreet)
        {            
            return housesInStreet;
        }

        public int TimeRoadCrossed(IList<House> housesInStreet)
        {
            var previousDirection = housesInStreet.FirstOrDefault().Direction;
            var timeCrosses = 0;
            foreach(var house in housesInStreet)
            {
                if(previousDirection != house.Direction)
                {
                    timeCrosses += 1;
                }

                previousDirection = house.Direction;
            }

            return timeCrosses;
        }
    }
}
