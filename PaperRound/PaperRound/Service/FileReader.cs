﻿using System;
using System.IO;

namespace PaperRound
{

    public class FileReader : IFileReader
    {
        public string ReadFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentNullException("Invalid file path.");
            }

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("No file exists at the provided location.");
            }

            return File.ReadAllText(filePath);
        }
    }
}