﻿using PaperRound.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PaperRound
{
    public class StreetParser : IStreetParser
    {

        /// <summary>
        /// Parse text and return a list of houses
        /// </summary>
        /// <param name="text">The actual text</param>
        /// <param name="delimitter">The delimitter house numbers are seperated by (Defaults to Space).</param>
        /// <returns>A list of houses.</returns>
        public Street ValidateAndParse(string text, char delimitter = ' ')
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException("Passed text is empty");
            }

            var street = new Street()
            {
                Houses = new List<House>()
            };

            var houses = text.Split(delimitter);
            var houseNumber = 0;
            foreach (var house in houses)
            {
                if (int.TryParse(house, out houseNumber))
                {
                    street.Houses.Add(new House { Number = houseNumber });
                }
            }

            // Validate Street File
            if(street.Houses.Count == 0)
            {
                throw new StreetParserException("Could not parse any houses");
            }
            
            if (street.Houses.First().Number != 1)
            {
                throw new StreetParserException("Houses must start from 1");
            }

            if (street.Houses.Select(o => o.Number).Distinct().Count() != street.Houses.Select(o => o.Number).Count())
            {
                throw new StreetParserException("Houses cannot be repeated");
            }

            if (street.HousesOnNorth.Count * street.HousesOnNorth.Count != street.HousesOnNorth.Sum(o => o.Number))
            {
                throw new StreetParserException("Incorrect sequence for on north facing houses");
            }

            if ((street.HousesOnSouth.Count * (street.HousesOnSouth.Count + 1)) != street.HousesOnSouth.Sum(o => o.Number))
            {
                throw new StreetParserException("Incorrect sequence for on south facing houses");
            }

            return street;
        }
    }
}