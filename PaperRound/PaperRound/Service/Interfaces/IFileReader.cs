﻿namespace PaperRound
{
    public interface IFileReader
    {
        string ReadFile(string filePath);
    }
}