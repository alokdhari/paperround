﻿namespace PaperRound
{
    public interface IReadFileResult
    {
        string ErrorMessage { get; set; }
        bool Success { get; set; }
    }
}