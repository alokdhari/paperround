﻿using System.Collections.Generic;

namespace PaperRound
{
    public interface IPaperDelivery
    {
        IList<House> GetDeliveryOrder(IList<House> housesInStreet);

        int TimeRoadCrossed(IList<House> housesInStreet);
    }
}
