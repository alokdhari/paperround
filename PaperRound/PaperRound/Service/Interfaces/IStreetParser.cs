﻿using System.Collections.Generic;

namespace PaperRound
{
    public interface IStreetParser
    {
        Street ValidateAndParse(string text, char delimitter = ' ');
    }
}