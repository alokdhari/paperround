﻿using System.Collections.Generic;

namespace PaperRound
{
    public interface ITownPlanner
    {
        Street GetStreetFromTextFile(string filePath);
    }
}