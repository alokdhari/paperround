﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.IO;

namespace PaperRound.Tests
{
    [TestFixture]
    public class TownPlannerTests
    {
        private ITownPlanner townPlanner;

        private string baseFilePath;

        public TownPlannerTests()
        {
            this.baseFilePath = TestContext.CurrentContext.TestDirectory + "\\TestFiles\\";
            townPlanner = new TownPlanner();
        }

        [Test]
        public void ShouldBeAbleToLoadAStreetFromAFilePath()
        {
            // Arrange
            var filePath = this.baseFilePath + "\\testSpecFile.txt";

            // Action
            var street = townPlanner.GetStreetFromTextFile(filePath);

            // Assert
            street.Should().NotBeNull();
            street.Houses.Count.Should().Be(14);
        }
    }
}
