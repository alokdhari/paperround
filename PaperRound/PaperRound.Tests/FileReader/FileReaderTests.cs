﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.IO;

namespace PaperRound.Tests
{
    [TestFixture]
    public class FileReaderTests
    {
        private IFileReader service;
        private string baseFilePath;

        public FileReaderTests()
        {
            this.baseFilePath = TestContext.CurrentContext.TestDirectory + "\\TestFiles\\";
            service = new FileReader();
        }

        [Test]
        public void ShouldGetStringFromFile()
        {
            // Arrange
            // Action
            var result = this.service.ReadFile(this.baseFilePath + "testSpecFile.txt");

            // Assert
            result.Should().NotBeNullOrEmpty();
        }

        [Test]        
        public void ShouldThrowExceptionIfFileNotFound()
        {
            Assert.Throws<FileNotFoundException>(() => this.service.ReadFile(this.baseFilePath + "NonExistantFile.txt"));
        }

        [Test]
        public void ShouldThrowExceptionIfInCorrectParameterIsProvided()
        {
            Assert.Throws<ArgumentNullException>(() => this.service.ReadFile(string.Empty));
        }
    }
}
