﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace PaperRound.Tests
{
    [TestFixture]
    public class DeliverPaperBySideOfStreetTest
    {
        private IPaperDelivery paperDelivery;
        
        public DeliverPaperBySideOfStreetTest()
        {
            this.paperDelivery = new DeliverPaperBySideOfStreet();
        }

        [Test]
        public void ShouldGetDeliveryOrder()
        {
            // Arrange
            var houses = new List<House>
            {
                new House { Number = 1 },
                new House { Number = 2 },
                new House { Number = 3 },
                new House { Number = 4 },
                new House { Number = 5 },
                new House { Number = 6 }
            };

            // Actions
            var deliveryOrder = this.paperDelivery.GetDeliveryOrder(houses);

            // Assert
            deliveryOrder.Should().NotBeNull();
            string.Join("", deliveryOrder.Select(o => o.Number).ToArray()).Should().Be("135642");
        }

        [Test]
        public void ShouldGetTimeRoadCrossed()
        {
            // Arrange
            var houses = new List<House>
            {
                new House { Number = 1 },
                new House { Number = 2 },
                new House { Number = 3 },
                new House { Number = 4 },
                new House { Number = 5 },
                new House { Number = 6 }
            };

            // Actions
            var timeRoadCrossed = this.paperDelivery.TimeRoadCrossed(houses);

            // Assert
            timeRoadCrossed.Should().Be(1);
        }
    }
}
