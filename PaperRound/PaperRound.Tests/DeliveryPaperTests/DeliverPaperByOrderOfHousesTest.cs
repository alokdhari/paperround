﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace PaperRound.Tests
{
    [TestFixture]
    public class DeliverPaperByOrderOfHousesTest
    {
        private IPaperDelivery paperDelivery;

        public DeliverPaperByOrderOfHousesTest()
        {
            this.paperDelivery = new DeliverPaperByOrderOfHouses();
        }

        [Test]
        public void ShouldGetDeliveryOrder()
        {
            // Arrange
            var houses = new List<House>
            {
                new House { Number = 1 },
                new House { Number = 2 },
                new House { Number = 3 },
                new House { Number = 4 },
                new House { Number = 5 },
                new House { Number = 6 }
            };

            // Actions
            var deliveryOrder = this.paperDelivery.GetDeliveryOrder(houses);

            // Assert
            deliveryOrder.Should().NotBeNull();
            string.Join("", deliveryOrder.Select(o => o.Number).ToArray()).Should().Be("123456");
        }

        [Test]
        public void ShouldGetTimeRoadCrossed()
        {
            // Arrange
            var houses = new List<House>
            {
                new House { Number = 1 },
                new House { Number = 2 },
                new House { Number = 3 },
                new House { Number = 4 },
                new House { Number = 5 },
                new House { Number = 6 }
            };

            // Actions
            var timeRoadCrossed = this.paperDelivery.TimeRoadCrossed(houses);

            // Assert
            timeRoadCrossed.Should().Be(5);
        }
    }
}
