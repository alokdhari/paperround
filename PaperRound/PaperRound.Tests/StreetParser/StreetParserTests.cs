﻿using FluentAssertions;
using NUnit.Framework;
using PaperRound.Exceptions;
using System;

namespace PaperRound.Tests
{
    [TestFixture]
    public class StreetParserTests
    {
        private IStreetParser service;
        private IFileReader fileReader;
        private string baseFilePath;

        public StreetParserTests()
        {
            this.service = new StreetParser();
            this.fileReader = new FileReader();
            this.baseFilePath = TestContext.CurrentContext.TestDirectory + "\\TestFiles\\";
        }

        public string GetValidFileText()
        {
            return this.fileReader.ReadFile(this.baseFilePath + "testSpecFile.txt");
        }

        [Test]
        public void ShouldReturnAListOfHouses()
        {
            // Arrange
            var validFileContents = this.fileReader.ReadFile(this.baseFilePath + "testSpecFile.txt");

            // Action
            var street = this.service.ValidateAndParse(validFileContents);

            // Assert
            street.Should().NotBeNull();
            street.Houses.Count.Should().Be(14);
        }

        [Test]
        public void ShouldThrowInvalidArgumentIfTextIsEmpty()
        {
            // Arrange
            var fileContents = string.Empty;

            // Action + Assert
            var exception = Assert.Throws<ArgumentException>(() => service.ValidateAndParse(fileContents));
            exception.Message.Should().Be("Passed text is empty");
        }

        [Test]
        public void ShouldFailForRepeatedHouses()
        {
            // Arrange
            var filePath = this.baseFilePath + "repeatedHouse.txt";
            var repeatedHouseFileContents = this.fileReader.ReadFile(filePath);

            // Action + Assert
            var exception = Assert.Throws<StreetParserException>(() => this.service.ValidateAndParse(repeatedHouseFileContents));
            exception.Message.Should().Be("Houses cannot be repeated");
        }

        [Test]
        public void ShouldFailForInvalidFile()
        {
            // Arrange
            var filePath = this.baseFilePath + "invalidFile.txt";
            var repeatedHouseFileContents = this.fileReader.ReadFile(filePath);

            // Action + Assert
            var exception = Assert.Throws<StreetParserException>(() => this.service.ValidateAndParse(repeatedHouseFileContents));
            exception.Message.Should().Be("Houses must start from 1");
        }

        [Test]
        public void ShouldFailForIncorrectSequenceInNorth()
        {
            // Arrange
            var filePath = this.baseFilePath + "incorrectNorth.txt";
            var repeatedHouseFileContents = this.fileReader.ReadFile(filePath);

            // Action + Assert
            var exception = Assert.Throws<StreetParserException>(() => this.service.ValidateAndParse(repeatedHouseFileContents));
            exception.Message.Should().Be("Incorrect sequence for on north facing houses");
        }

        [Test]
        public void ShouldFailForIncorrectSequenceInSouth()
        {
            // Arrange
            var filePath = this.baseFilePath + "incorrectSouth.txt";
            var repeatedHouseFileContents = this.fileReader.ReadFile(filePath);

            // Action + Assert
            var exception = Assert.Throws<StreetParserException>(() => this.service.ValidateAndParse(repeatedHouseFileContents));
            exception.Message.Should().Be("Incorrect sequence for on south facing houses");
        }
    }
}
