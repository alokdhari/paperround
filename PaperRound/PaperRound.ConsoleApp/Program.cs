﻿using PaperRound.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaperRound.ConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to paper round.");
            Console.WriteLine("To exit type exit or press 1 to input file");
            var selectedOption = string.Empty;
            while (selectedOption != "quit")
            {
                var filePath = DisplayMessageAndGetInput("Input the file path for the street specification file.");
                var townPlanner = new TownPlanner();
                var deliverByOrderOfHouses = new DeliverPaperByOrderOfHouses();
                var deliverPaperBySideOfStreet = new DeliverPaperBySideOfStreet();

                try
                {
                    var street = townPlanner.GetStreetFromTextFile(filePath);
                    Console.WriteLine("");
                    Console.WriteLine("File Successfully parsed \n");
                    Console.WriteLine("Houses on street: " + street.Houses.Count);
                    Console.WriteLine("Houses on north of street: " + street.HousesOnNorth.Count);
                    Console.WriteLine("Houses on south of street: " + street.HousesOnSouth.Count);

                    Console.WriteLine("\nDelivering paper by order of houses");

                    Console.WriteLine("Delivery Order: " + string.Join(" - ", deliverByOrderOfHouses.GetDeliveryOrder(street.Houses).Select(o => o.Number.ToString()).ToArray()));
                    Console.WriteLine("Times Street Crossed: " + deliverByOrderOfHouses.TimeRoadCrossed(street.Houses));

                    Console.WriteLine("\nDelivering paper by side of street");

                    Console.WriteLine("Delivery Order: " + string.Join(" - ", deliverPaperBySideOfStreet.GetDeliveryOrder(street.Houses).Select(o => o.Number.ToString()).ToArray()));
                    Console.WriteLine("Times Street Crossed: " + deliverPaperBySideOfStreet.TimeRoadCrossed(street.Houses));
                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (StreetParserException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.WriteLine("\nThank You for running the Town Planner app. \n");

                selectedOption = DisplayMessageAndGetInput("Type 'quit' and press enter to quit OR press enter to input file again.");
            }
        }

        public static string DisplayMessageAndGetInput(string message)
        {
            Console.WriteLine(message + " \n");
            Console.WriteLine("waiting for input.. \n");
            return Console.ReadLine();
        }
    }
}
